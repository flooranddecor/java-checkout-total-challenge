package com.floordecor.springbootchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootchallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootchallengeApplication.class, args);
	}

}
