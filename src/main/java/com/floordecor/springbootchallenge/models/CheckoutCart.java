package com.floordecor.springbootchallenge.models;

import java.util.List;

public class CheckoutCart {
    
    /***
     * TODO: Determine desired data structure for storing rules.
     *       Must store at least what is in the product pricing table in the readme.md 
     *       Don't forget to change the constructor type to match.
     */
    private Object rules;

    public CheckoutCart(Object rules) {
        this.rules = rules;
    }

    public static final CheckoutCart buildCheckoutCartWithDefaultRules(){
        /***
         * TODO: Create the default price rules data structure based on the table in the readme.md
         */
        return new CheckoutCart(null);
    }


    /***
     * TODO: Create implement functions to scan and getTotal based on rules.
     */

    public void scan(String productCode) {
        // TODO: implement me
    }

    public double getTotal() {
        // TODO: implement me
        return 0.0;
    }

    
    /**
     * Returns the total after scanning each productCode in productCodes.
     * 
     * @param productCodes list of productCodes to scan.
     * @return the CheckoutCart total
     */
    public static final double priceCheck(List<String> productCodes) {
        CheckoutCart checkoutCart = CheckoutCart.buildCheckoutCartWithDefaultRules();

        productCodes.forEach((productCode)->{
            checkoutCart.scan(productCode);
        });

        return checkoutCart.getTotal();
    }

}