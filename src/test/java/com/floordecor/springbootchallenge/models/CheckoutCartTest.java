package com.floordecor.springbootchallenge.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class CheckoutCartTest {

    @Test
	void testPriceCheckTotals () { 
        assertEquals( 0, CheckoutCart.priceCheck(List.of()));
        assertEquals( 50, CheckoutCart.priceCheck(List.of("A")));
        assertEquals( 80, CheckoutCart.priceCheck(List.of("A", "B")));
        assertEquals(115, CheckoutCart.priceCheck(List.of("C","D","B","A")));

        assertEquals(100, CheckoutCart.priceCheck(List.of("A","A")));
        assertEquals(130, CheckoutCart.priceCheck(List.of("A","A","A")));
        assertEquals(180, CheckoutCart.priceCheck(List.of("A","A","A","A")));
        assertEquals(230, CheckoutCart.priceCheck(List.of("A","A","A","A","A")));
        assertEquals(260, CheckoutCart.priceCheck(List.of("A","A","A","A","A","A")));

        assertEquals(160, CheckoutCart.priceCheck(List.of("A","A","A","B")));
        assertEquals(175, CheckoutCart.priceCheck(List.of("A","A","A","B","B")));
        assertEquals(190, CheckoutCart.priceCheck(List.of("A","A","A","B","B","D")));
        assertEquals(190, CheckoutCart.priceCheck(List.of("D","A","B","A","B","A")));
    }

    @Test
    void testIncrementalScanning() {
        CheckoutCart checkoutCart = CheckoutCart.buildCheckoutCartWithDefaultRules();

        assertEquals(  0, checkoutCart.getTotal());

        checkoutCart.scan("A");  
        assertEquals( 50, checkoutCart.getTotal());
        
        checkoutCart.scan("B");  
        assertEquals( 80, checkoutCart.getTotal());
        
        checkoutCart.scan("A");  
        assertEquals(130, checkoutCart.getTotal());
        
        checkoutCart.scan("A");  
        assertEquals(160, checkoutCart.getTotal());
        
        checkoutCart.scan("B");  
        assertEquals(175, checkoutCart.getTotal());

    }
}